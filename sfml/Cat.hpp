#pragma once
#include "Pet.hpp"

namespace myGame
{

	class Cat : public Pet
	{
	public:
		Cat(int x, int y) : Pet(x,y)
		{

		}

		virtual bool LoadTextures()
		{
			if (!m_texturePetRight.loadFromFile("assets\\cat_walk_right.png"))
			{
				std::cerr << "Loading cat error." << std::endl;
				return false;
			}

			if (!m_texturePetRightNext.loadFromFile("assets\\cat_walk_right_2.png"))
			{
				std::cerr << "Loading cat error." << std::endl;
				return false;
			}

			if (!m_texturePetLeft.loadFromFile("assets\\cat_walk_left.png"))
			{
				std::cerr << "Loading cat error." << std::endl;
				return false;
			}

			if (!m_texturePetLeftNext.loadFromFile("assets\\cat_walk_left_2.png"))
			{
				std::cerr << "Loading cat error." << std::endl;
				return false;
			}

			m_spritePet.setTexture(m_texturePetRight);
			m_spritePet.setPosition(m_x, m_y);
			m_spritePet.setOrigin(m_texturePetRight.getSize().x / 2, m_texturePetRight.getSize().y / 2);

			return true;
		}

		virtual bool LoadSounds()
		{
			if (!m_soundBuffer.loadFromFile("music\\myau.wav"))
			{
				std::cerr << "Loading myau error." << std::endl;
				return false;
			}

			m_sound.setBuffer(m_soundBuffer);
			m_sound.setPitch(.5f);

			return true;
		}
	};

}
