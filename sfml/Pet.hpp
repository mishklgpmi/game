#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

namespace myGame
{
	enum class Direction
	{
		Left,
		Right
	};

	class Pet
	{
	public:
		Pet(int x, int y) 
		{
			m_x = x;
			m_y = y;
		}
		~Pet() {}

		virtual bool LoadTextures() = 0;
		virtual bool LoadSounds() = 0;

		sf::Sprite Get()
		{
			return m_spritePet;
		}

		void Say()
		{
			m_sound.play();
		}

		void Move(Direction dir)
		{
			if (dir == Direction::Right)
			{
				m_x += m_velocity;
				if (m_frameNumber == 0)
				{
					m_frameNumber = 1;
					m_spritePet.setTexture(m_texturePetRight);
				}
				else
				{
					m_frameNumber = 0;
					m_spritePet.setTexture(m_texturePetRightNext);
				}
			}
			else if (dir == Direction::Left)
			{
				m_x -= m_velocity;
				if (m_frameNumber == 0)
				{
					m_frameNumber = 1;
					m_spritePet.setTexture(m_texturePetLeft);
				}
				else
				{
					m_frameNumber = 0;
					m_spritePet.setTexture(m_texturePetLeftNext);
				}
			}
		}

		void Jump()
		{
			m_vy = -m_velocity;
		}

		void Update()
		{
			if (m_vy < 10)
				m_vy++;

			m_y += m_vy;

			if (m_y > 800)
				m_y = 800;

			m_spritePet.setPosition(m_x, m_y);
		}

	protected:
		int m_frameNumber = 0;
		int m_x, m_y;
		int m_vx = 0, m_vy = 0;
		float m_velocity = 10;

		// �������
		sf::Sprite m_spritePet;
		sf::Texture m_texturePetRight;
		sf::Texture m_texturePetRightNext;
		sf::Texture m_texturePetLeft;
		sf::Texture m_texturePetLeftNext;
		sf::SoundBuffer m_soundBuffer;
		sf::Sound m_sound;

	};
}
