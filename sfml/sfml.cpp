﻿#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <thread>
#include <iostream>
#include "PetFactory.hpp"

#define PI acos(-1)

int main()
{
    myGame::Pet* pet = myGame::createPet("Cat");
    if (pet == nullptr)
    {
        return 0;
    }

    sf::RenderWindow window(sf::VideoMode(1300, 866), "Destroyeer!");
   
    sf::Music music;
    if (!music.openFromFile("music\\back.ogg"))
        return -1; // error
    sf::Music::TimeSpan span(sf::seconds(0), sf::seconds(4.4));
    music.setLoopPoints(span);
    music.setLoop(true);
    music.setVolume(20);
    music.play();

    sf::Texture background;
    if (!background.loadFromFile("assets\\background.jpg"))
    {
        std::cerr << "Loading background error." << std::endl;
    }

    sf::Sprite backgroundSprite;
    backgroundSprite.setTexture(background);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
        {
            pet->Say();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
        {
            pet->Jump();
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            pet->Move(myGame::Direction::Right);
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            pet->Move(myGame::Direction::Left);
        }

        pet->Update();

        window.clear();
        window.draw(backgroundSprite);
        window.draw(pet->Get());
        window.display();

        std::this_thread::sleep_for(std::chrono::milliseconds(16));
    }

    return 0;
}