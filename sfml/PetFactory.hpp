#pragma once
#include "Pet.hpp"
#include "Cat.hpp"

namespace myGame
{

	Pet* createPet(std::string name)
	{
		Pet* pet = nullptr;
		if (name == "Cat")
			pet = new Cat(200, 800);
		//else if (name == "Dog")
		//	pet = new Dog(200, 800);

		if (!pet->LoadTextures())
			return nullptr;
		if (!pet->LoadSounds())
			return nullptr;

		return pet;
	}

}
